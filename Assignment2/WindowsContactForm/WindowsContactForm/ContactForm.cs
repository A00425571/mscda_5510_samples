﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Windows.Forms;
// Project -> Add references
using Microsoft.VisualBasic.FileIO;

namespace WindowsContactForm
{
    public partial class ContactForm : Form
    {
        Dictionary<int, Customer> allData = new Dictionary<int, Customer>();
        int currentIndex = 0;
        int maxIndex = 0;

        public ContactForm()
        {
            InitializeComponent();
        }

        private void prevButtonClick(object sender, EventArgs e)
        {
            currentIndex = currentIndex - 1;
            UpdateData(currentIndex);
        }

        private void nextButtonClick(object sender, EventArgs e)
        {
            currentIndex = currentIndex + 1;
            UpdateData(currentIndex);
        }

        private void ContactForm_Load(object sender, EventArgs e)
        {
            allData.Clear();

            using (CustomerDataModel db = new CustomerDataModel())
            {
                DbSet<Customer> customers = db.Customers;

                foreach (Customer customer in customers)
                {
                    allData.Add(maxIndex, customer);
                    maxIndex++;
                }
            }
            UpdateData(currentIndex);
        }

        private void UpdateData(int index)
        {
            Customer personData = null;
            if (allData.TryGetValue(index, out personData))
            {
                this.textBoxFirstName.Text = personData.FirstName;
                this.textBoxID.Text = index.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            DialogResult result = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (result == DialogResult.OK)
            {
                textImportFile.Text = openFileDialog1.FileName;
            }
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            String file = @"C: \Users\dpenny\Documents\Source\Repos\MSCDA_5510_Samples\Assignment2\WindowsContactForm\WindowsContactForm\names.csv";
            //String file = textImportFile.Text;
            using (TextFieldParser parser = new TextFieldParser(file))
            {
                parser.Delimiters = new string[] { "," };
                while (true)
                {
                    string[] parts = parser.ReadFields();
                    if (parts == null)
                    {
                        break;
                    }
                    using (CustomerDataModel db = new CustomerDataModel())
                    {
                        
                        DbSet<Customer> customers = db.Customers;
                        Customer cust = new Customer();
                        cust.FirstName = parts[0];
                        // this saves it in the DB to be saved when Save is called
                        customers.Add(cust);
                        db.SaveChanges();
                        allData.Add(maxIndex, cust);
                        maxIndex++;
                    }
                }
            }
            UpdateData(currentIndex);
        }

        private void textImportFile_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
