﻿//  File ContactForm
// 
// Assignment #2
// Due Feb 12th
//
// Requirement 1: Expand on this form to display information in database that displays the following fields
// First Name (TextBox) - max lenght 50
// Last Name (TextBox) - max lenght 50
// Street Number (TextBox) - number
// Address (TextBox) - max lenght 50
// City (TextBox) - max lenght 50
// Province (TextBox) - max lenght 50
// Country (TextBox) - Text no validation - Canada
// Postal Code  (TextBox) - Max 7 characters ( 6 plus space)
// Phone Number (TextBox)
// email Address (TextBox)
//
// Database will allow fields that exceed validation requiremernts above
// Validation will be executed as we use the next previous buttons
//
//
// Add Next and Prevous Buttons to navigate through the database ( handle index 0 and max index)
// Display the current primary key of the database in a textbox or label
// Add a Status TextBox and dispaly any validation errors that are encoutered, 
// If multiple errors exist only show one.

// Requirement 2: Expand on the below example to create a import the contents of the CSV file 
// created in Assignment1, read the data into entity classes and save data to database.  
// After import Next and Prev buttons should work.
//
// TODO for Dan - add example of how to save data
//
// Please always try to write clean And readable code
// Here Is a good reference doc http://ricardogeek.com/docs/clean_code.html  
// Submit to Bitbucket under Assignment2

namespace WindowsContactForm
{
    partial class ContactForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.prevButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.textImportFile = new System.Windows.Forms.TextBox();
            this.importLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonImport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Primary Key";
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(96, 22);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(100, 20);
            this.textBoxID.TabIndex = 1;
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(96, 49);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(100, 20);
            this.textBoxFirstName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "First Name";
            // 
            // prevButton
            // 
            this.prevButton.Location = new System.Drawing.Point(12, 87);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(75, 23);
            this.prevButton.TabIndex = 4;
            this.prevButton.Text = "Previous";
            this.prevButton.UseVisualStyleBackColor = true;
            this.prevButton.Click += new System.EventHandler(this.prevButtonClick);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(110, 87);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 5;
            this.nextButton.Text = "Next";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButtonClick);
            // 
            // textImportFile
            // 
            this.textImportFile.Location = new System.Drawing.Point(96, 163);
            this.textImportFile.Name = "textImportFile";
            this.textImportFile.ReadOnly = true;
            this.textImportFile.Size = new System.Drawing.Size(243, 20);
            this.textImportFile.TabIndex = 6;
            // 
            // importLabel
            // 
            this.importLabel.AutoSize = true;
            this.importLabel.Location = new System.Drawing.Point(13, 166);
            this.importLabel.Name = "importLabel";
            this.importLabel.Size = new System.Drawing.Size(82, 13);
            this.importLabel.TabIndex = 7;
            this.importLabel.Text = "Import CSV File:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(345, 160);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonImport
            // 
            this.buttonImport.Location = new System.Drawing.Point(96, 190);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(75, 23);
            this.buttonImport.TabIndex = 9;
            this.buttonImport.Text = "Import";
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // ContactForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 243);
            this.Controls.Add(this.buttonImport);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.importLabel);
            this.Controls.Add(this.textImportFile);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.prevButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxFirstName);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.label1);
            this.Name = "ContactForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.ContactForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button prevButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.TextBox textImportFile;
        private System.Windows.Forms.Label importLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonImport;
    }
}

